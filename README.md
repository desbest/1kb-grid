# 1KB Grid

Want to use a CSS grid but don't feel like going back to college? Join the club.

Other CSS frameworks try to do everything—grid system, style reset, basic typography, form styles. But complex systems are, well, complex. Looking for a simple, lightweight approach that doesn't require a PhD? Meet The 1KB CSS Grid.

Though simple, The 1KB CSS Grid packs the punch of a heavyweight. Nested rows? Piece of cake! A fluid grid rather than a fixed one? No problem! Explores some of the finer details of implementing the grid.

[More information in this blog post](https://usabilitypost.com/2009/05/29/the-1kb-css-grid-part-1/).

The successor of 1KB Grid is the [Semantic Grid System](https://desbest.com/semantic-grid-system/)

[You can view the official 1KB Grid website here](https://desbest.com/1kbgrid/)

## FAQ

**What do the numbers in the file name mean?**

They refer to number of columns, column width and gutter width.

## Credits

Tyler Tate is a user experience designer at [Nutshell](http://www.nutshell.com/) and [TwigKit](http://www.twigkit.com/). The successor to 1KB Grid is Semantic Grid System

## License

GPL License and MIT License
